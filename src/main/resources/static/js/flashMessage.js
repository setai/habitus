(function () {
    const note = document.getElementsByClassName("flash")[0];

    if (typeof note != 'undefined') {
        note.classList.add("flash-active");

        setTimeout(function () {
            note.classList.remove("flash-active");
        }, 4000);
    }
}());

