(function () {
    var dels = document.getElementsByClassName('http-verb-delete');
    for (var i = 0; i < dels.length; i++) {
        dels[i].addEventListener('click', ajax);
    }

    function ajax(event) {
        event.preventDefault();
        // Create content on Bootstrap Modal component.
        var modal = document.getElementById("id_confirm");
        var modalContent =
                '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                + '<span aria-hidden="true">×</span>'
                + '</button>'
                + '<h4 class="modal-title" id="gridModalLabel">' + 'Are you sure?' + '</h4>'
                + '</div>'
                + '<div class="modal-footer">'
                + '<button id="id_truebtn" type="button" class="btn btn-danger">Yes</button>'
                + '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>'
                + '</div>';

        var modalInstance = new Modal(modal, {
            content: modalContent, // sets modal content
            backdrop: 'static', // we don't want to dismiss Modal when Modal or backdrop is the click event target
            keyboard: false // we don't want to dismiss Modal on pressing Esc key
        });
        modalInstance.show();

        document.getElementById("id_truebtn").onclick = function () {
            //do your delete operation
            do_delete(event);
        };
    }

    function do_delete(event) {
        var request = new XMLHttpRequest();
        var href = event.target.getAttribute('href');
        console.log(event.target);
        var redirect = event.target.dataset.redirect; // redirect is read from the data-redirect -attribute.
        var reload = event.target.dataset.reload; // true or false, like the Refresh button

        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                //window.location = ''; // refresh view by redirecting to the same page.
                window.location.replace(redirect);
                if (reload.toString().toLowerCase() === 'true') {
                    location.reload();
                    window.scrollTo(window.scrollX, window.scrollY - 100); //translate anchor some pixels up from where it is linked to
                }
            }
        };

        request.open('DELETE', href, true);
        request.setRequestHeader('Content-Type', 'text/plain; charset=UTF-8');
        request.send();
    }
}());
