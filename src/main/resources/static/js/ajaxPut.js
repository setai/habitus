(function () {
    var puts = document.getElementsByClassName('http-verb-put');
    for (var i = 0; i < puts.length; i++) {
        puts[i].addEventListener('click', ajax);
    }

    function ajax(event) {
        event.preventDefault();
        var request = new XMLHttpRequest();
        var href = this.getAttribute('href');
        var redirect = this.dataset.redirect; // redirect is read from the data-redirect -attribute.
        var formid = this.dataset.formid; // id of the form is read from the data-formid -attribute. 
        var reload = this.dataset.reload; // true or false, like the Refresh button
        var form = document.getElementById(formid).elements;

        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                window.location.replace(redirect); // refresh view by redirecting back
                if (reload.toString().toLowerCase() === 'true') {
                    location.reload();
                    window.scrollTo(window.scrollX, window.scrollY - 100); //translate anchor some pixels up from where it is linked to
                }
            }
        };

        var data = {};

        for (var i = 0; i < form.length; i++) {
            var name = form.item(i).getAttribute("name");
            data[name] = form.item(i).value;
        }

        var json = JSON.stringify(data);

        request.open('PUT', href, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.send(json);
    }
}());
