(function () {
    var posts = document.getElementsByClassName('http-verb-post');
    for (var i = 0; i < posts.length; i++) {
        posts[i].addEventListener('click', ajax);
    }

    function ajax(event) {
        event.preventDefault();
        var href = this.getAttribute('href');
        var request = new XMLHttpRequest();
        var redirect = this.dataset.redirect; // redirect is read from the data-redirect -attribute.
        var reload = this.dataset.reload; // true or false, like the Refresh button

        var json = JSON.stringify({});

        if (typeof this.dataset.formid !== 'undefined') {
            var form = document.getElementById(this.dataset.formid).elements;
            var data = {};
            for (var i = 0; i < form.length; i++) {
                var name = form.item(i).getAttribute("name");
                data[name] = form.item(i).value;
            }
            json = JSON.stringify(data);
        }

        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                window.location.replace(redirect); // refresh view by redirecting back
                if (reload.toString().toLowerCase() === 'true') {
                    location.reload();
                    window.scrollTo(window.scrollX, window.scrollY - 100); //translate anchor some pixels up from where it is linked to
                }
            }
        };

        request.open('POST', href, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.send(json);
    }
}());
