package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.entity.Account;
import project.entity.Attentional;
import project.entity.LikeEntity;

public interface LikeRepository extends JpaRepository<LikeEntity, Long> {

    long countBySubject(Attentional attentional);
    long countBySubjectAndActor(Attentional attentional, Account actor);
}
