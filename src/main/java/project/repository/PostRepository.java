package project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import project.entity.Account;
import project.entity.Comment;
import project.entity.Post;

public interface PostRepository extends JpaRepository<Post, Long> {

    Page<Post> findByOwner(Account account, Pageable pageable);

}
