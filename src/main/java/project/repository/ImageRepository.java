package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import project.entity.Image;

public interface ImageRepository extends JpaRepository<Image, Long> {

}