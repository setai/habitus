package project.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(HttpServletRequest request, Model model) {
        String referrer = request.getHeader("Referer");
        request.getSession().setAttribute("url_prior_login", referrer);
        model.addAttribute("page", "login");
        return "/login";
    }

    @GetMapping("/login-error")
    public RedirectView loginError(RedirectAttributes atts) {
        List<String> messages = new ArrayList<>();
        messages.add("Please check your login details and try again.");
        atts.addFlashAttribute("messages", messages);
        
        return new RedirectView("/login");
    }

    @GetMapping("/afterlogout")
    public String afterLogout() {
        return "redirect:/accounts";
    }
}
