package project.controller;

import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import project.entity.Account;
import project.service.AccountService;

@Controller
public class FriendRequestController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts/{url}/requests/new")
    public String invite(@RequestParam String id, HttpServletRequest request) {
        Account currentUser = accountService.currentUser();
        Long longId = Long.parseLong(id);
        if (Objects.nonNull(currentUser) && accountService.idExists(longId)) {
            accountService.addFriend(currentUser, accountService.getOne(longId));
        }

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @GetMapping("/accounts/{url}/requests/{id}")
    public String confirmFriend(@PathVariable String url, @PathVariable String id, HttpServletRequest request) {
        Account currentUser = accountService.currentUser();
        Long longId = Long.parseLong(id);

        if (currentUser.getUrl().equals(url) && accountService.idExists(longId)) {
            accountService.confirmFriend(currentUser, accountService.getOne(longId));
        }

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }

    @ResponseBody
    @DeleteMapping("/accounts/{url}/requests/{id}")
    public String removeFriend(@PathVariable String url, @PathVariable String id, HttpServletRequest request) {
        Account currentUser = accountService.currentUser();
        Long longId = Long.parseLong(id);
        if (currentUser.getUrl().equals(url) && accountService.idExists(longId)) {
            accountService.removeFriend(currentUser, accountService.getOne(longId));
        }

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }
}
