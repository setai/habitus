package project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import project.pojo.TransferPost;
import project.entity.Account;
import project.entity.Mate;
import project.entity.Post;
import project.service.AccountService;
import project.service.PostService;

@Controller
public class WallController {

    @Autowired
    private PostService postService;

    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts/{url}/posts")
    public String index(@PathVariable String url, Model model) {
        Account wallOwner = accountService.findUserByUrl(url);
        Account currentUser = accountService.currentUser();
        Boolean isFriend = false;

        if (Objects.nonNull(currentUser)) {
            if (wallOwner == currentUser) {
                isFriend = true;
            } else {
                Mate mate = accountService.isFriend(wallOwner, currentUser);
                isFriend = mate.getStatus() == Mate.Status.CONFIRMED ? true : false;
            }
        }

        model.addAttribute("currentUser", currentUser);
        model.addAttribute("wallOwner", wallOwner);
        model.addAttribute("isFriend", isFriend);
        model.addAttribute("url", url);
        model.addAttribute("posts", postService.getMany(wallOwner, 25));
        //model.addAttribute("posts", wallOwner.getPosts());
        return "posts/index";
    }

    @GetMapping("/accounts/{url}/posts/new")
    public String register(@PathVariable String url, Model model) {
        model.addAttribute("page", "post");
        model.addAttribute("currentUser", accountService.currentUser());
        model.addAttribute("wallOwner", accountService.findUserByUrl(url));
        return "posts/new";
    }

    @GetMapping("/accounts/{url}/posts/{id}/edit")
    public String edit(@PathVariable String url, @PathVariable String id, Model model) {
        Long longId = Long.parseLong(id);
        Post post = postService.getOne(longId);
        model.addAttribute("currentUser", accountService.currentUser());
        model.addAttribute("wallOwner", accountService.findUserByUrl(url));
        model.addAttribute("page", "post");
        model.addAttribute("post", post);

        return "posts/edit";
    }

    @ResponseBody
    @PutMapping("/accounts/{url}/posts/{id}")
    public String update(@PathVariable String url,
            @PathVariable String id,
            @RequestBody TransferPost postForm
    ) {

        Long longId = Long.parseLong(id);
        postService.update(longId, postForm.getTitle(), postForm.getMessage());

        return "redirect:/accounts/" + url + "/posts";
    }

    @PostMapping("/accounts/{url}/posts")
    public RedirectView add(
            @PathVariable String url,
            @RequestParam String title,
            @RequestParam String message,
            RedirectAttributes atts) {

        List<String> messages = new ArrayList<>();
        RedirectView redirectView = new RedirectView();

        Account wallOwner = accountService.findUserByUrl(url);
        Account currentUser = accountService.currentUser();

        postService.create(wallOwner, currentUser, title, message);
        redirectView.setUrl("/accounts/" + url + "/posts");

        messages.add("Your post is published.");
        atts.addFlashAttribute("messages", messages);

        return redirectView;
    }

    @ResponseBody
    @DeleteMapping("/accounts/{url}/posts/{id}")
    public String removeIfPermitted(@PathVariable String url,
            @PathVariable String id,
            HttpServletRequest request) {
        Account currentUser = accountService.currentUser();
        Long longId = Long.parseLong(id);

        postService.destroyIfPermitted(longId, currentUser, 1);

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }
}
