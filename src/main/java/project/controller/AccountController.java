package project.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import project.entity.Account;
import project.service.AccountService;

@Controller
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts")
    public String list(@RequestParam(required = false) String query, Model model) {
        model.addAttribute("presentations", accountService.getAccountPresentations(query));
        model.addAttribute("currentUser", accountService.currentUser());
        model.addAttribute("page", "accounts");

        model.addAttribute("query", query);
        return "accounts/index";
    }

    @GetMapping("/accounts/new")
    public String register(Model model) {
        model.addAttribute("page", "register");
        return "accounts/new";
    }

    @GetMapping("/accounts/{url}/edit")
    public String edit(@PathVariable String url, Model model) {
        model.addAttribute("page", "register");

        Account currentUser = accountService.findUserByUrl(url);
        model.addAttribute("currentUser", currentUser);

        return "accounts/edit";
    }

    @PostMapping("/accounts")
    public RedirectView add(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String forename,
            @RequestParam String surname,
            @RequestParam String url,
            RedirectAttributes atts) {

        List<String> messages = new ArrayList<>();
        RedirectView redirectView = new RedirectView();

        if (accountService.usernameExists(username)) {
            messages.add("Username already taken");
        }

        if (accountService.urlExists(url)) {
            messages.add("Profile name already taken");
        }

        if (messages.isEmpty()) {
            accountService.save(username, password, forename, surname, url);
            messages.add("Congratulations, your account is created. Please, login now.");
            redirectView.setUrl("/login");
        } else {
            atts.addFlashAttribute("username", username);
            atts.addFlashAttribute("forename", forename);
            atts.addFlashAttribute("surname", surname);
            atts.addFlashAttribute("url", url);
            redirectView.setUrl("/accounts/new");
        }

        atts.addFlashAttribute("messages", messages);

        return redirectView;
    }

    // Should be Put, but we use Post here.
    @PostMapping("/accounts/{url}")
    public String update(@ModelAttribute Account account, @PathVariable String url) {
        Account currentUser = accountService.findUserByUrl(url);
        if (account.getUrl().equals(currentUser.getUrl())) {
            accountService.update(account);
        }
        return "redirect:/accounts";
    }

    public static String makeUrl(HttpServletRequest request) {
        return request.getRequestURL().toString() + "?" + request.getQueryString();
    }
}
