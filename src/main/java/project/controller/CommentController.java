package project.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import project.entity.Account;
import project.entity.Post;
import project.pojo.TransferComment;
import project.service.AccountService;
import project.service.CommentService;
import project.service.PostService;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PostService postService;

    @ResponseBody
    @PostMapping("/accounts/{url}/posts/{id}/comments")
    public String add(
            @PathVariable String url,
            @PathVariable String id,
            @RequestBody TransferComment commentForm
    ) {

        Long longId = Long.parseLong(id);
        Post post = postService.getOne(longId);
        Account currentUser = accountService.currentUser();

        commentService.create(post, currentUser, commentForm.getComment());

        return "redirect:/accounts/" + url + "/posts";
    }

    @ResponseBody
    @PutMapping("/accounts/{url}/posts/{postId}/comments/{commentId}")
    public String update(
            @PathVariable String url,
            @PathVariable String commentId,
            @RequestBody TransferComment commentForm
    ) {

        System.out.println(commentForm);

        Long longId = Long.parseLong(commentId);
        commentService.update(longId, commentForm.getComment());

        return "redirect:/accounts/" + url + "/posts";
    }

    @ResponseBody
    @DeleteMapping("/accounts/{url}/posts/{postId}/comments/{commentId}")
    public String removeIfPermitted(
            @PathVariable String url,
            @PathVariable String commentId,
            HttpServletRequest request) {
        Account currentUser = accountService.currentUser();
        Long longId = Long.parseLong(commentId);

        commentService.destroyIfPermitted(longId, currentUser, 1);

        String referer = request.getHeader("Referer");
        return "redirect:" + referer;
    }
}
