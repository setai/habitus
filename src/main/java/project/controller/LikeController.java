package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import project.entity.Account;
import project.entity.Post;
import project.service.LikeService;
import project.service.PostService;
import project.service.AccountService;

@Controller
public class LikeController {

    @Autowired
    private LikeService likeService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PostService postService;

    @ResponseBody
    @PostMapping("/accounts/{url}/posts/{id}/likes")
    public String add(
            @PathVariable String url,
            @PathVariable String id
    ) {

        Long longId = Long.parseLong(id);
        Post post = postService.getOne(longId);
        Account currentUser = accountService.currentUser();

        likeService.create(post, currentUser);

        return "redirect:/accounts/" + url + "/posts";
    }
}
