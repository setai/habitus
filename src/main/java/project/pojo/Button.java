package project.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Button {

    public static enum Epiteth {
        BTN_INVITE,
        BTN_CANCEL,
        BTN_CONFIRM,
        BTN_IGNORE,
        BTN_UNFRIEND,
        BTN_ME
    }

    private Epiteth epiteth;
    private String text;
    private String link;
}
