package project.pojo;

import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import project.entity.Account;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountPresentation {

    private Account account;
    private String description;
    private LocalDateTime dateTime;
    private List<Button> buttons;
}
