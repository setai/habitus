package project.entity;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class LikeEntity extends Opinion {

    public LikeEntity(Attentional subject, Account actor) {
        super(subject, actor);
    }
}
