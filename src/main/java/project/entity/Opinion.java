package project.entity;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Opinion extends AbstractPersistable<Long> {

    private LocalDateTime createdOn = LocalDateTime.now();

    @ManyToOne
    private Attentional subject;

    @ManyToOne
    private Account actor;

    public Opinion(Attentional subject, Account actor) {
        this.subject = subject;
        this.actor = actor;
    }

}
