package project.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.NaturalIdCache;

@Entity
@NaturalIdCache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Account {

    @Id
    @GeneratedValue
    private Long id;

    // TODO: https://stackoverflow.com/questions/17092601/how-to-validate-unique-username-in-spring
    //@Column(unique = true)
    //@NotEmpty
    @NaturalId
    private String username;
    //@NotEmpty
    private String password;
    //@NotEmpty
    private String forename;
    //@NotEmpty
    private String surname;
    //@Column(unique = true)
    //@NotEmpty
    private String url;
    //private Image  image;

    public Account() {
    }

    @OneToMany(
            mappedBy = "fromFriend",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Mate> friends = new ArrayList<>();

    @OneToMany(
            mappedBy = "withFriend",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Mate> befriends = new ArrayList<>();

    @OneToMany(mappedBy = "owner")
    private List<Post> posts = new ArrayList<>();

    @OneToMany(mappedBy = "owner")
    private List<Image> images = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(username, account.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        String output = "------------- Account --------------\n";
        output += "id=" + id + ", ";
        output += "username=" + username + ", ";
        output += "forename=" + forename + ", ";
        output += "surname=" + surname + ", ";
        output += "url=" + url + ", ";
        output += "friends size=" + friends.size() + ", ";
        output += "befriends size=" + befriends.size() + "\n";
        output += "------------------------------------\n";
        return output;

    }

    public Account(String username, String password, String forename, String surname, String url) {
        this.username = username;
        this.password = password;
        this.forename = forename;
        this.surname = surname;
        this.url = url;
        this.friends = new ArrayList<Mate>();
        this.befriends = new ArrayList<Mate>();
    }

    // Getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Mate> getFriends() {
        return friends;
    }

    public void setFriends(List<Mate> friends) {
        this.friends = friends;
    }

    public List<Mate> getBefriends() {
        return befriends;
    }

    public void setBefriends(List<Mate> befriends) {
        this.befriends = befriends;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

}
