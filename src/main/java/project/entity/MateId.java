package project.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MateId implements Serializable {

    @Column
    private Long fromId;

    @Column
    private Long withId;

    private MateId() {
    }

    public MateId(Long fromId, Long withId) {
        this.fromId = fromId;
        this.withId = withId;
    }

    public Long getIdFriend() {
        return fromId;
    }

    public Long getIdBefriend() {
        return withId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MateId that = (MateId) o;
        return Objects.equals(fromId, that.fromId)
                && Objects.equals(withId, that.withId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromId, withId);
    }
}
