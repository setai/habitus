package project.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Attentional extends AbstractPersistable<Long> {

    private LocalDateTime createdOn = LocalDateTime.now();

    @ManyToOne
    private Account actor;

    @ManyToOne
    private Account owner;

    /*
    Likes are simple. The count per subject only matters.
    We do not bother to map likes on their Attentional subject. 
    Instead we just save likes in LikeRepository and count them there.
    */
    /*
    @OneToMany(mappedBy = "subject", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Like> likes = new ArrayList<>();
     */
    
    @OneToMany(mappedBy = "subject", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Comment> comments = new ArrayList<>();

    public Attentional(Account owner, Account actor) {
        this.owner = owner;
        this.actor = actor;
    }

}
