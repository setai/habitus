package project.entity;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Post extends Attentional {

    //@NotBlank(message="Title may not be empty") //Form takes care of validation
    private String title;

    //@NotBlank(message="Message may not be empty") //Form takes care of validation
    private String message;

    public Post(Account owner, Account user, String title, String message) {
        super(owner, user);
        this.title = title;
        this.message = message;
    }

}
