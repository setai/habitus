package project.entity;

import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment extends Opinion {

    private String comment;

    public Comment(Attentional subject, Account actor, String comment) {
        super(subject, actor);
        this.comment = comment;
    }
}
