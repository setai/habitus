/* TODO: Implement Image gallery in Habitus.
At the moment Image entity is not used for anything.
*/

package project.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Image extends Attentional {

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] content;
    

}