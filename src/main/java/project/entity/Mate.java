package project.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
//@Table
public class Mate {

    public static enum Status {
        NONE,
        PENDING,
        CONFIRMED
    }

    @EmbeddedId
    private MateId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("fromId")
    private Account fromFriend;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("withId")
    private Account withFriend;

    private LocalDateTime createdOn = LocalDateTime.now();
    private Status status;

    public Mate() {
    }

    public Mate(Account friend, Account befriend) {
        this(friend, befriend, Status.PENDING);
    }

    public Mate(Account friend, Account befriend, Status status) {
        this.fromFriend = friend;
        this.withFriend = befriend;
        this.status = status;
        this.id = new MateId(friend.getId(), befriend.getId());
    }

    //Getters and setters
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public MateId getId() {
        return id;
    }

    public void setId(MateId id) {
        this.id = id;
    }

    public Account getFriend() {
        return fromFriend;
    }

    public void setFriend(Account friend) {
        this.fromFriend = friend;
    }

    public Account getBefriend() {
        return withFriend;
    }

    public void setBefriend(Account befriend) {
        this.withFriend = befriend;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Mate that = (Mate) o;
        return Objects.equals(fromFriend, that.fromFriend)
                && Objects.equals(withFriend, that.withFriend);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromFriend, withFriend);
    }

    @Override
    public String toString() {
        String output = "=============== Mate ===============\n";
        output += "Friend:   " + fromFriend.toString();
        output += "Befriend: " + withFriend.toString();
        output += "====================================\n";
        return output;
    }
}
