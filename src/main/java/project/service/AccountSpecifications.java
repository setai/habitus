package project.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import project.entity.Account;
import project.entity.Account_;

final class AccountSpecifications {

    private AccountSpecifications() {
    }

    static Specification<Account> forenameOrSurnameContainsIgnoreCase(Set<String> names) {
        return (root, query, cb) -> {

            Path<String> forenamePath = root.get(Account_.forename); // root.get("forename") without metamodel class Account_
            Path<String> surnamePath = root.get(Account_.surname); // root.<String>get(Account_.surname)

            String containsLikePattern;
            List<Predicate> predicates = new ArrayList<>();
            for (String name : names) {
                containsLikePattern = getContainsLikePattern(name);
                predicates.add(cb.like(cb.lower(forenamePath), containsLikePattern));
                predicates.add(cb.like(cb.lower(surnamePath), containsLikePattern));
            }

            query.orderBy(cb.asc(surnamePath), cb.asc(forenamePath));
            return cb.or(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    private static String getContainsLikePattern(String searchTerm) {
        if (searchTerm == null || searchTerm.isEmpty()) {
            return "%";
        } else {
            return "%" + searchTerm.toLowerCase() + "%";
        }
    }
}
