package project.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import project.entity.Account;
import project.entity.Mate;
import project.pojo.AccountPresentation;
import project.pojo.Button;
import project.repository.AccountRepository;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Account getOne(Long id) {
        return accountRepository.getOne(id);
    }

    public Account currentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        // no-security profile that ignors authentication returns auth == null ---------
        if (auth == null) {
            return null;
        }
        // ------------------------------------------------------------------------------------------
        return accountRepository.findByUsername(auth.getName());
    }

    public boolean idExists(Long id) {
        return accountRepository.existsById(id);
    }

    public boolean usernameExists(String username) {
        if (accountRepository.findByUsername(username) != null) {
            return true;
        }
        return false;
    }

    public boolean urlExists(String url) {
        if (accountRepository.findByUrl(url) != null) {
            return true;
        }
        return false;
    }

    public Account findUserByUrl(String url) {
        return accountRepository.findByUrl(url);
    }

    public void update(Account account) {
        Account current = findUserByUrl(account.getUrl());
        current.setForename(account.getForename());
        current.setSurname(account.getSurname());
        current.setPassword(passwordEncoder.encode(account.getPassword()));
        this.save(current);
    }

    public void save(Account account) {
        accountRepository.save(account);
    }

    public void save(String username, String password, String forename, String surname, String url) {
        accountRepository.save(new Account(username, passwordEncoder.encode(password), forename, surname, url));
    }

    @Transactional
    public void addFriend(Account friend, Account befriend) {
        createFriend(friend, befriend, Mate.Status.PENDING);
    }

    @Transactional
    public void removeFriend(Account friend, Account befriend) {
        deleteFriend(friend, befriend);
    }

    @Transactional
    public void confirmFriend(Account currentUser, Account friend) {
        deleteFriend(currentUser, friend);
        // Flushing required here.
        // Reason: Mate's embedded id = (friendId, currentUserId). 
        // Id=(currentUserId, friendId) is different and accepted, 
        // Id=(friendId, currentUserId) when used another time without flushing crashes.
        accountRepository.saveAndFlush(currentUser);
        createFriend(friend, currentUser, Mate.Status.CONFIRMED);
    }

    private void createFriend(Account friend, Account befriend, Mate.Status status) {
        Mate mate = new Mate(friend, befriend, status);
        friend.getFriends().add(mate);
        befriend.getBefriends().add(mate);
    }

    private void deleteFriend(Account friend, Account befriend) {
        Boolean done = false;
        Mate mate;

        // Cancel request
        for (Iterator<Mate> iterator = friend.getFriends().iterator(); iterator.hasNext();) {
            mate = iterator.next();

            if (mate.getBefriend().getUsername().equals(befriend.getUsername())) {
                iterator.remove();
                friend.getFriends().remove(mate);
                befriend.getBefriends().remove(mate);
                done = true;
            }
        }

        // Ignore request
        if (!done) {
            for (Iterator<Mate> iterator = friend.getBefriends().iterator(); iterator.hasNext();) {
                mate = iterator.next();

                if (mate.getFriend().getUsername().equals(befriend.getUsername())) {
                    iterator.remove();
                    friend.getBefriends().remove(mate);
                    befriend.getFriends().remove(mate);
                }
            }
        }
    }

    private Set<String> splitNames(String names) {
        Set<String> nameSet = new HashSet<>();
        List<String> nameList = Arrays.asList(names.split(" "));
        nameSet.addAll(nameList);
        return nameSet;
    }

    private List<Account> findUsersByName(String names) {
        List<Account> accounts = new ArrayList<>();

        if (Objects.nonNull(names)) {
            Set<String> nameSet = splitNames(names);
            Specification<Account> specification = AccountSpecifications.forenameOrSurnameContainsIgnoreCase(nameSet);
            accounts = accountRepository.findAll(specification);
        } else {
            // Maximum number of results from findAll() is limited to 10.
            Pageable onePage = PageRequest.of(0, 10);
            accounts = accountRepository.findAll((Specification<Account>) null, onePage).getContent();
        }

        return accounts;
    }

    public List<AccountPresentation> getAccountPresentations(String query) {
        Account currentUser = this.currentUser();
        List<AccountPresentation> accountPresentations = new ArrayList<>();
        List<Account> accounts = this.findUsersByName(query);

        for (Account account : accounts) {
            List<Button> buttons = new ArrayList<>();
            LocalDateTime dateTime = LocalDateTime.now();

            if (currentUser == null) {
                accountPresentations.add(new AccountPresentation(account, "Login to meet your friends", dateTime, buttons));
            } else if (currentUser == account) {
                String link = "/accounts/" + currentUser.getUrl() + "/posts";
                buttons.add(new Button(Button.Epiteth.BTN_ME, "Look at me", link));
                accountPresentations.add(new AccountPresentation(account, "Hey, it's me!", dateTime, buttons));
            } else {
                Mate mate = isFriend(currentUser, account);

                String link;
                switch (mate.getStatus()) {
                    case NONE:
                        link = "/accounts/" + currentUser.getUrl() + "/requests/new?id=" + account.getId();
                        buttons.add(new Button(Button.Epiteth.BTN_INVITE, "Add as Friend", link));
                        accountPresentations.add(new AccountPresentation(account, "You could be friends?", dateTime, buttons));
                        break;
                    case PENDING:
                        if (mate.getFriend() == currentUser) {
                            link = "/accounts/" + currentUser.getUrl() + "/requests/" + account.getId();
                            buttons.add(new Button(Button.Epiteth.BTN_CANCEL, "Cancel Request", link));
                            accountPresentations.add(new AccountPresentation(account, "Your friend request is pending since", dateTime, buttons));
                        } else {
                            link = "/accounts/" + currentUser.getUrl() + "/requests/" + account.getId();
                            buttons.add(new Button(Button.Epiteth.BTN_CONFIRM, "Confirm", link));
                            buttons.add(new Button(Button.Epiteth.BTN_IGNORE, "Ignore", link));
                            accountPresentations.add(new AccountPresentation(account, "Accept a friend request from this user sent on", dateTime, buttons));
                        }
                        break;
                    case CONFIRMED:
                        link = "/accounts/" + currentUser.getUrl() + "/requests/" + account.getId();
                        buttons.add(new Button(Button.Epiteth.BTN_UNFRIEND, "Unfriend", link));
                        accountPresentations.add(new AccountPresentation(account, "You are friends.", dateTime, buttons));
                        break;
                    default:
                }
            }
        }
        return accountPresentations;
    }

    public Mate isFriend(Account friend, Account befriend) {
        Mate value = new Mate(friend, befriend, Mate.Status.NONE);
        for (Iterator<Mate> iterator = friend.getFriends().iterator(); iterator.hasNext();) {
            Mate mate = iterator.next();

            if (mate.getBefriend().equals(befriend)) {
                iterator.remove();
                value = mate;
            }
        }

        // If friendship was not found in one way, try search again another way round.
        if (value.getStatus() == Mate.Status.NONE) {
            for (Iterator<Mate> iterator = friend.getBefriends().iterator(); iterator.hasNext();) {
                Mate mate = iterator.next();

                if (mate.getFriend().equals(befriend)) {
                    iterator.remove();
                    value = mate;
                }
            }
        }

        return value;
    }

}
