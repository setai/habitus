package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.entity.LikeEntity;
import project.entity.Account;
import project.entity.Attentional;
import project.repository.LikeRepository;

@Service
public class LikeService {

    @Autowired
    private LikeRepository likeRepository;

    public boolean liked(Attentional subject, Account actor) {
        if (likeRepository.countBySubjectAndActor(subject, actor) == 0) {
            return false;
        }
        return true;
    }

    public void create(Attentional subject, Account actor) {
        if (!liked(subject, actor)) {
            likeRepository.save(new LikeEntity(subject, actor));
        }
    }

    public String count(Attentional subject) {
        Long c = likeRepository.countBySubject(subject);
        return c.toString();
    }
}
