package project.service;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import project.entity.Account;
import project.entity.Post;
import project.repository.PostRepository;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public void create(Account owner, Account user, String title, String message) {
        postRepository.save(new Post(owner, user, title, message));
    }

    public void update(Long id, String title, String message) {
        Post post = postRepository.getOne(id);
        post.setTitle(title);
        post.setMessage(message);
        postRepository.save(post);
    }

    public boolean idExists(Long id) {
        return postRepository.existsById(id);
    }

    public List<Post> getMany(Account owner, int n) {
        Pageable pageable = PageRequest.of(0, n, Sort.by("createdOn").descending());
        return postRepository.findByOwner(owner, pageable).getContent();
    }

    public Post getOne(Long id) {
        return postRepository.getOne(id);
    }

    public void destroyIfPermitted(Long id, Account currentUser, int timeFrameInHours) {
        if (idExists(id)) {
            Post post = getOne(id);
            if (currentUser == post.getOwner()
                    || currentUser == post.getActor()
                    && post.getCreatedOn().isAfter(LocalDateTime.now().minusHours(timeFrameInHours))) {

                // Delete a post
                // Comments and Likes are deleted all together because of Attentional's orphanRemoval and CascadeType.
                postRepository.delete(post);
            }
        }
    }

}
