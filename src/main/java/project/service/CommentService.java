package project.service;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import project.entity.Account;
import project.entity.Attentional;
import project.entity.Comment;
import project.entity.Post;
import project.repository.CommentRepository;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    public boolean idExists(Long id) {
        return commentRepository.existsById(id);
    }

    public void create(Post subject, Account actor, String comment) {
        commentRepository.save(new Comment(subject, actor, comment));
    }

    public void update(Long id, String message) {
        Comment comment = commentRepository.getOne(id);
        comment.setComment(message);
        commentRepository.save(comment);
    }

    public Comment getOne(Long id) {
        return commentRepository.getOne(id);
    }

    public List<Comment> getMany(Attentional attentional, int n) {
        Pageable pageable = PageRequest.of(0, n, Sort.by("createdOn").descending());
        return commentRepository.findBySubject(attentional, pageable).getContent();
    }

    public void destroyIfPermitted(Long id, Account currentUser, int timeFrameInHours) {
        if (idExists(id)) {
            Comment comment = getOne(id);
            if (currentUser == comment.getSubject().getOwner()
                    || currentUser == comment.getActor()
                    && comment.getCreatedOn().isAfter(LocalDateTime.now().minusHours(timeFrameInHours))) {

                commentRepository.delete(comment);
            }
        }
    }

}
